module.exports = {
  "plugins": [
    "react",
    "react-native"
  ],
  "extends": [
    "plugin:react-native/all",
    "airbnb",
    "airbnb/hooks"
  ],
  "parserOptions": {
      "ecmaFeatures": {
          "jsx": true
      }
  },
  "env": {
    "react-native/react-native": true
  },
  "parser": "babel-eslint",
  "rules": {
    "linebreak-style": 0,
    "react/prop-types": 0,
    "react/jsx-filename-extension": 0,
    "import/prefer-default-export": 0,
    "react/jsx-props-no-spreading": 0,
    "func-names": 0,
    "no-shadow": 0,
    "react-hooks/exhaustive-deps": 0,
    "comma-dangle": ["error", "never"]
  }
};