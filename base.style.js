import { StyleSheet } from 'react-native';

export const colors = {
  error: '#ef3412',
  primary: '#196ee2',
  secondary: '#e8eaf6',
  border: '#555',
  fontLight: 'white',
  loaderWrapper: '#0007'
};

export const baseStyle = StyleSheet.create({
  alignText: {
    textAlign: 'center'
  },
  centered: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  error: {
    color: colors.error,
    fontSize: 20
  },
  flex: {
    flex: 1
  },
  italic: {
    fontStyle: 'italic'
  }
});
