const API_URL = 'https://jsonkeeper.com';

async function callApi(endpoint, method) {
  const url = API_URL + endpoint;
  const options = {
    method
  };

  return fetch(url, options)
    .then((response) => (response.ok ? response.json() : Promise.reject(Error('Failed to load'))))
    .catch((error) => {
      throw error;
    });
}

export function getContacts() {
  return callApi('/b/SH0W', 'GET');
}

export { callApi };
