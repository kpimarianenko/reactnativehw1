import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import ContactsList from '../../pages/ContactsList';
import { routeNames } from '../routeNames';
import ContactPage from '../../pages/ContactPage';

const Stack = createStackNavigator();

function MainNavigator() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        options={{ headerShown: false }}
        name={routeNames.main.CONTACTS_LIST}
        component={ContactsList}
      />
      <Stack.Screen
        options={{ headerShown: false }}
        name={routeNames.main.CONTACT}
        component={ContactPage}
      />
    </Stack.Navigator>
  );
}

export default MainNavigator;
