import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Login from '../../pages/Login';
import { routeNames } from '../routeNames';

const Stack = createStackNavigator();

function LoginNavigator() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        options={{ headerShown: false }}
        name={routeNames.LOGIN}
        component={Login}
      />
    </Stack.Navigator>
  );
}

export default LoginNavigator;
