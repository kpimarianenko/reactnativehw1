import React from 'react';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { useSelector } from 'react-redux';
import LoginNavigator from './StackNavigator/LoginNavigator';
import MainNavigator from './StackNavigator/MainNavigator';

function AppNavigator() {
  const user = useSelector((state) => state.auth.user);
  return (
    <NavigationContainer>
      {user ? <MainNavigator /> : <LoginNavigator />}
    </NavigationContainer>
  );
}

export default AppNavigator;
