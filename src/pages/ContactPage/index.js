import React, { useEffect, useState } from 'react';
import { connect, useSelector } from 'react-redux';
import {
  View, Image, KeyboardAvoidingView, Platform, ScrollView, Alert, Text
} from 'react-native';
import { baseStyle } from '../../../base.style';
import AvaPlaceholder from '../../assets/images/ava.png';
import ContactField from './ContactField';
import ContactButton from './ContactButton';
import { style } from './style';
import { createContact, editContact, deleteContact } from '../ContactsList/actions';
import * as actions from './actions';

function ContactPage({
  navigation, route, setEditableContact, editContact, createContact, deleteContact
}) {
  const { contacts } = useSelector((state) => state.contactsList);
  const id = route.params?.id;
  const [editMode, setEditMode] = useState(!id);
  const { editableContact } = useSelector((state) => state.contactPage);

  const [error, setError] = useState('');
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [phone, setPhone] = useState('');

  const setDefault = () => {
    setName(editableContact.name);
    setEmail(editableContact.email);
    setPhone(editableContact.phone);
  };

  const toggleEditMode = () => {
    setEditMode(!editMode);
  };

  const cancelEdit = () => {
    setEditMode(!editMode);
    setDefault();
  };

  const backToList = () => {
    navigation.goBack();
  };

  const validate = () => {
    if (name.length <= 0) return false;
    if (!email.match(/^\S+@\S+\.\S+$/)) return false;
    if (!phone.match(/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})([ .-]?)([0-9]{4})/)) return false;
    return true;
  };

  const showConfirmAlert = (title, onConfirm, onCancel = () => {}) => {
    Alert.alert(title, 'Are you sure?',
      [
        {
          text: 'No',
          onPress: onCancel,
          style: 'cancel'
        },
        { text: 'Yes', onPress: onConfirm }
      ],
      { cancelable: false });
  };

  const onContactEdit = () => {
    showConfirmAlert('Edit contact', () => {
      if (validate()) {
        editContact({
          id,
          name,
          email,
          phone
        });
        setError('');
        toggleEditMode();
      } else {
        setError('Data is incorrect');
      }
    });
  };

  const onContactCreate = () => {
    if (validate()) {
      createContact({
        name,
        email,
        phone
      });
      backToList();
    } else {
      setError('Data is incorrect');
    }
  };

  const onContactDelete = () => {
    showConfirmAlert('Delete contact', () => {
      deleteContact(id);
      backToList();
    });
  };

  const onChangeName = (value) => {
    setName(value);
  };

  const onChangePhone = (value) => {
    setPhone(value);
  };

  const onChangeEmail = (value) => {
    setEmail(value);
  };

  useEffect(() => {
    const contact = contacts.find((c) => c.id === id);
    setName(contact?.name || '');
    setEmail(contact?.email || '');
    setPhone(contact?.phone || '');
    setEditableContact(contact);
  }, [id]);

  return (
    <KeyboardAvoidingView style={baseStyle.flex} behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
      <ScrollView contentContainerStyle={style.page}>
        <Image style={style.ava} source={AvaPlaceholder} />

        <ContactField
          onChangeText={onChangeName}
          editMode={editMode}
          value={name}
          title="Name"
        />
        <ContactField
          onChangeText={onChangePhone}
          editMode={editMode}
          value={phone}
          title="Phone number"
        />
        <ContactField
          onChangeText={onChangeEmail}
          editMode={editMode}
          value={email}
          title="Email"
        />

        <View style={style.controls}>
          {editableContact
            ? (
              <>
                {editMode
                  ? (
                    <>
                      <ContactButton title="Save" onPress={onContactEdit} />
                      <ContactButton title="Cancel" onPress={cancelEdit} />
                    </>
                  )
                  : (
                    <>
                      <ContactButton title="Edit" onPress={toggleEditMode} />
                      <ContactButton title="Delete" onPress={onContactDelete} />
                    </>
                  )}
              </>
            )
            : (
              <>
                <ContactButton title="Create" onPress={onContactCreate} />
                <ContactButton title="Cancel" onPress={backToList} />
              </>
            )}
        </View>
        <Text style={baseStyle.error}>{error}</Text>
      </ScrollView>
    </KeyboardAvoidingView>
  );
}

const mapDispatchToProps = {
  ...actions,
  createContact,
  editContact,
  deleteContact
};

export default connect(null, mapDispatchToProps)(ContactPage);
