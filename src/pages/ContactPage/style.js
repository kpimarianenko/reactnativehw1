import { Dimensions, StyleSheet } from 'react-native';
import { colors } from '../../../base.style';

const { width } = Dimensions.get('window');

export const style = StyleSheet.create({
  ava: {
    height: width * 0.3,
    width: width * 0.3
  },
  buttonText: {
    color: colors.fontLight,
    fontSize: 16,
    textAlign: 'center'
  },
  controls: {
    flexDirection: 'row'
  },
  controlsButton: {
    backgroundColor: colors.primary,
    borderColor: colors.fontLight,
    borderRadius: 10,
    borderWidth: 1,
    flex: 1,
    margin: '10%',
    padding: 10
  },
  field: {
    marginBottom: 15,
    width: '100%'
  },
  header: {
    fontSize: 20,
    fontWeight: 'bold'
  },
  input: {
    borderBottomColor: colors.border,
    borderBottomWidth: 2,
    lineHeight: 15,
    padding: 10
  },
  page: {
    alignItems: 'center',
    flex: 1,
    padding: 20
  }
});
