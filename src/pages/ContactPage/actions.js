import { SET_EDITABLE_CONTACT } from './actionsTypes';

export const setEditableContact = (contact) => ({
  type: SET_EDITABLE_CONTACT,
  payload: {
    contact
  }
});
