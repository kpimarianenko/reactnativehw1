import React from 'react';
import { TouchableOpacity, Text } from 'react-native';
import { style } from './style';

function ContactButton({ title, ...attrs }) {
  return (
    <TouchableOpacity {...attrs} style={style.controlsButton}>
      <Text style={style.buttonText}>
        {title}
      </Text>
    </TouchableOpacity>
  );
}

export default ContactButton;
