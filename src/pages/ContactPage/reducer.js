import { SET_EDITABLE_CONTACT } from './actionsTypes';

const initialState = {
  editableContact: null
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_EDITABLE_CONTACT: {
      const { contact } = action.payload;
      return {
        ...state,
        editableContact: contact
      };
    }
    default: {
      return state;
    }
  }
}
