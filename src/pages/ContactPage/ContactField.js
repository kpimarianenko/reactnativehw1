import React from 'react';
import { View, Text, TextInput } from 'react-native';
import { style } from './style';
import { baseStyle } from '../../../base.style';

function ContactField({
  title, value, editMode, ...attrs
}) {
  return (
    <View style={style.field}>
      <Text style={style.header}>{title}</Text>
      {editMode
        ? (
          <TextInput
            {...attrs}
            value={value}
            style={[style.input, editMode ? baseStyle.italic : {}]}
          />
        )
        : <Text style={style.input}>{value}</Text>}
    </View>
  );
}

export default ContactField;
