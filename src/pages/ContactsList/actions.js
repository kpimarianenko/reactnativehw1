import {
  SET_CONTACTS_SUCCESS,
  SET_CONTACTS_FAILURE,
  SEARCH,
  SHOW_LOADER,
  HIDE_LOADER,
  CREATE_CONTACT,
  EDIT_CONTACT,
  DELETE_CONTACT
} from './actionTypes';

export const setContactsSuccess = (contacts) => ({
  type: SET_CONTACTS_SUCCESS,
  payload: {
    contacts
  }
});

export const setContactsFailure = (error) => ({
  type: SET_CONTACTS_FAILURE,
  payload: {
    error
  }
});

export const search = (searchStr) => ({
  type: SEARCH,
  payload: {
    searchStr
  }
});

export const showLoader = () => ({
  type: SHOW_LOADER,
  payload: {
    isLoaded: false
  }
});

export const hideLoader = () => ({
  type: HIDE_LOADER,
  payload: {
    isLoaded: true
  }
});

export const createContact = (data) => ({
  type: CREATE_CONTACT,
  payload: {
    data
  }
});

export const editContact = (data) => ({
  type: EDIT_CONTACT,
  payload: {
    data
  }
});

export const deleteContact = (id) => ({
  type: DELETE_CONTACT,
  payload: {
    id
  }
});
