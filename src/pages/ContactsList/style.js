import { StyleSheet } from 'react-native';
import { colors } from '../../../base.style';

export const style = StyleSheet.create({
  add: {
    backgroundColor: colors.primary,
    borderRadius: 100,
    bottom: 0,
    height: 45,
    margin: 15,
    position: 'absolute',
    right: 0,
    width: 45
  },
  list: {
    flexGrow: 1
  },
  listWrapper: {
    padding: 10
  }
});
