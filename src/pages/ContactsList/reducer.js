import {
  SET_CONTACTS_SUCCESS,
  SET_CONTACTS_FAILURE,
  SEARCH,
  SHOW_LOADER,
  HIDE_LOADER,
  CREATE_CONTACT,
  EDIT_CONTACT,
  DELETE_CONTACT
} from './actionTypes';

const initialState = {
  contacts: [],
  displayedContacts: [],
  error: '',
  isLoaded: false
};

function createNewID(contacts) {
  return contacts[contacts.length - 1].id + 1 || 0;
}

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_CONTACTS_SUCCESS: {
      const { contacts } = action.payload;
      return {
        ...state,
        contacts,
        displayedContacts: contacts
      };
    }
    case SET_CONTACTS_FAILURE: {
      const { error } = action.payload;
      return {
        ...state,
        error
      };
    }
    case SEARCH: {
      const { searchStr } = action.payload;
      const { contacts } = state;
      const result = contacts.filter(({ name }) => {
        const lowerName = name.toLowerCase();
        const lowerSearch = searchStr.toLowerCase();
        return lowerName.includes(lowerSearch);
      });
      return {
        ...state,
        displayedContacts: result
      };
    }
    case SHOW_LOADER:
    case HIDE_LOADER: {
      const { isLoaded } = action.payload;
      return {
        ...state,
        isLoaded
      };
    }
    case CREATE_CONTACT: {
      const { data } = action.payload;
      const { contacts } = state;
      contacts.push({
        id: createNewID(contacts),
        ...data
      });
      return {
        ...state,
        contacts
      };
    }
    case EDIT_CONTACT: {
      const { data } = action.payload;
      const { contacts } = state;
      const updatedContacts = contacts.map((c) => {
        if (c.id === data.id) {
          return data;
        }
        return c;
      });
      return {
        ...state,
        contacts: updatedContacts
      };
    }
    case DELETE_CONTACT: {
      const { id } = action.payload;
      const { contacts } = state;
      return {
        ...state,
        contacts: contacts.filter((c) => c.id !== id)
      };
    }
    default: {
      return state;
    }
  }
}
