import React, { useEffect } from 'react';
import { FlatList, View, Text } from 'react-native';
import { connect, useSelector } from 'react-redux';
import { getContacts } from '../../helpers/apiHelper';
import Contact from '../../components/Contact';
import Search from '../../components/Search';
import * as actions from './actions';
import { style } from './style';
import AddContact from './AddContact';
import Loader from '../../components/Loader';
import { baseStyle } from '../../../base.style';
import { routeNames } from '../../routes/routeNames';

function ContactsList({
  navigation, setContactsSuccess,
  setContactsFailure, hideLoader
}) {
  const { displayedContacts, isLoaded, error } = useSelector((state) => state.contactsList);

  const renderItem = ({ item }) => (
    <Contact
      onPress={() => navigation.navigate(routeNames.main.CONTACT, { id: item.id })}
      contact={item}
    />
  );

  useEffect(() => {
    getContacts()
      .then((contacts) => {
        setContactsSuccess(contacts);
        hideLoader();
      })
      .catch(() => {
        setContactsFailure('Oops... something went wrong :c\nCheck your internet connection');
      });
  }, [hideLoader, setContactsFailure, setContactsSuccess]);

  if (error) {
    return (
      <View style={[baseStyle.flex, baseStyle.centered]}>
        <Text style={baseStyle.alignText}>{error}</Text>
      </View>
    );
  }
  return (
    isLoaded ? (
      <View style={[baseStyle.flex, style.listWrapper]}>
        <Search />
        { !(displayedContacts.length <= 0)
          ? (
            <FlatList
              style={style.list}
              data={displayedContacts}
              renderItem={renderItem}
              keyExtractor={(contact) => contact.id.toString()}
            />
          )
          : <Text style={baseStyle.alignText}>{'You don\'t have any contacts yet'}</Text>}
        <AddContact onPress={() => navigation.navigate(routeNames.main.CONTACT)} />
      </View>
    ) : <Loader />
  );
}

const mapDispatchToProps = {
  ...actions
};

export default connect(null, mapDispatchToProps)(ContactsList);
