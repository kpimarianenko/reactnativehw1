import React from 'react';
import { TouchableHighlight, Image } from 'react-native';
import AddImg from '../../assets/images/add.png';
import { style } from './style';
import { baseStyle } from '../../../base.style';

function AddContact({ onPress }) {
  return (
    <TouchableHighlight onPress={onPress} style={[style.add, baseStyle.centered]}>
      <Image source={AddImg} />
    </TouchableHighlight>
  );
}

export default AddContact;
