import React from 'react';
import { ScrollView, KeyboardAvoidingView, Platform } from 'react-native';
import Logo from '../../components/Logo';
import LoginForm from '../../components/LoginForm';
import { style } from './style';
import { baseStyle } from '../../../base.style';

function Login() {
  return (
    <KeyboardAvoidingView style={baseStyle.flex} behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
      <ScrollView contentContainerStyle={style.page}>
        <Logo />
        <LoginForm />
      </ScrollView>
    </KeyboardAvoidingView>
  );
}

export default Login;
