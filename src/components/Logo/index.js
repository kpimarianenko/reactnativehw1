import React from 'react';
import { Image, Text } from 'react-native';
import LogoImg from '../../assets/images/logo.png';
import { style } from './style';

function Logo() {
  return (
    <>
      <Image style={style.logo} source={LogoImg} />
      <Text style={style.name}>Contact book</Text>
    </>
  );
}

export default Logo;
