import { Dimensions, StyleSheet } from 'react-native';

const { width } = Dimensions.get('window');

export const style = StyleSheet.create({
  logo: {
    height: width * 0.3,
    width: width * 0.3
  },
  name: {
    fontSize: 20,
    textAlign: 'center'
  }
});
