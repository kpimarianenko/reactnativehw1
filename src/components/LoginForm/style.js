import { StyleSheet } from 'react-native';
import { colors } from '../../../base.style';

export const style = StyleSheet.create({
  formControl: {
    marginVertical: 10
  },
  header: {
    fontSize: 20
  },
  input: {
    borderColor: colors.border,
    borderRadius: 5,
    borderWidth: 1,
    fontSize: 20
  },
  wrapper: {
    padding: 15,
    width: '100%'
  }
});
