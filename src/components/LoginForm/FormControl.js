import React from 'react';
import { Text, View, TextInput } from 'react-native';
import { style } from './style';

function FormControl({ title, ...attrs }) {
  return (
    <View style={style.formControl}>
      <Text>{title}</Text>
      <TextInput {...attrs} style={style.input} />
    </View>
  );
}

export default FormControl;
