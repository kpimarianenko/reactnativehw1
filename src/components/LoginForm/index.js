import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Text, View, Button } from 'react-native';
import { style } from './style';
import FormControl from './FormControl';
import * as actions from './actions';
import { colors } from '../../../base.style';

function LoginForm({ login }) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const onLogin = () => {
    login(email, password);
  };

  const onChangeEmail = (value) => {
    setEmail(value);
  };

  const onChangePassword = (value) => {
    setPassword(value);
  };

  return (
    <View style={style.wrapper}>
      <Text style={style.header}>Sign in</Text>
      <View>
        <FormControl onChangeText={onChangeEmail} title="Email" />
        <FormControl secureTextEntry onChangeText={onChangePassword} title="Password" />
        <Button color={colors.primary} onPress={onLogin} title="Log in" />
      </View>
    </View>
  );
}

const mapDispatchToProps = {
  ...actions
};

export default connect(null, mapDispatchToProps)(LoginForm);
