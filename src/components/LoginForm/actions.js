import { LOGIN } from './actionsTypes';

export const login = (email, password) => ({
  type: LOGIN,
  payload: {
    email,
    password
  }
});
