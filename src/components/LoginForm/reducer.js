import { LOGIN } from './actionsTypes';

const initialState = {
  user: null,
  users: [
    {
      email: 'user',
      password: 'qwerty'
    }
  ]
};

export default function (state = initialState, action) {
  switch (action.type) {
    case LOGIN: {
      const { email, password } = action.payload;
      const user = state.users.filter((u) => u.email === email && u.password === password);
      return {
        ...state,
        user: user[0] || null
      };
    }
    default: {
      return state;
    }
  }
}
