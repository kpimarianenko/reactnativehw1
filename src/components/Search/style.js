import { StyleSheet } from 'react-native';
import { colors } from '../../../base.style';

export const style = StyleSheet.create({
  button: {
    backgroundColor: colors.primary,
    borderRadius: 100,
    fontSize: 24,
    height: 45,
    width: 45
  },
  input: {
    flex: 1,
    fontSize: 18,
    height: 40,
    lineHeight: 24
  },
  search: {
    alignItems: 'center',
    borderColor: colors.border,
    borderRadius: 35,
    borderWidth: 1,
    flexDirection: 'row',
    marginBottom: 10,
    padding: 3
  }
});
