import React, { useState, useEffect } from 'react';
import { connect, useSelector } from 'react-redux';
import {
  View, TouchableHighlight, TextInput, Image
} from 'react-native';
import SearchImg from '../../assets/images/search.png';
import { search } from '../../pages/ContactsList/actions';
import { style } from './style';
import { baseStyle } from '../../../base.style';

function Search({ search }) {
  const [searchStr, setSearchStr] = useState('');
  const { contacts } = useSelector((state) => state.contactsList);

  const onSearch = () => {
    search(searchStr);
  };

  const onChangeHandler = (value) => {
    setSearchStr(value);
  };

  useEffect(() => {
    onSearch();
  }, [contacts, contacts.length]);

  return (
    <View style={style.search}>
      <TextInput style={style.input} onChangeText={onChangeHandler} value={searchStr} />
      <TouchableHighlight onPress={onSearch} style={[style.button, baseStyle.centered]}>
        <Image source={SearchImg} />
      </TouchableHighlight>
    </View>
  );
}

const mapDispatchToProps = {
  search
};

export default connect(null, mapDispatchToProps)(Search);
