import { StyleSheet } from 'react-native';
import { colors } from '../../../base.style';

export const style = StyleSheet.create({
  wrapper: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: colors.loaderWrapper,
    flex: 1,
    zIndex: 1
  }
});
