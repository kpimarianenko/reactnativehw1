import React from 'react';
import { View, ActivityIndicator } from 'react-native';
import { baseStyle, colors } from '../../../base.style';
import { style } from './style';

function Loader() {
  return (
    <View style={[baseStyle.centered, style.wrapper]}>
      <ActivityIndicator size="large" color={colors.primary} />
    </View>
  );
}

export default Loader;
