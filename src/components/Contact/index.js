import React from 'react';
import {
  Text, View, Image, TouchableOpacity
} from 'react-native';
import { style } from './style';
import AvaPlaceholder from '../../assets/images/ava.png';

function Contact({ onPress, contact }) {
  const {
    name, phone, email
  } = contact;

  return (
    <TouchableOpacity onPress={onPress} style={style.contact}>
      <Image style={style.ava} source={AvaPlaceholder} />
      <View>
        <Text style={style.header}>{name}</Text>
        <Text style={style.text}>{phone}</Text>
        <Text style={style.text}>{email}</Text>
      </View>
    </TouchableOpacity>
  );
}

export default Contact;
