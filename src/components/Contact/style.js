import { StyleSheet } from 'react-native';
import { colors } from '../../../base.style';

const gap = 10;

export const style = StyleSheet.create({
  ava: {
    height: 40,
    marginRight: gap,
    width: 40
  },
  contact: {
    backgroundColor: colors.secondary,
    borderRadius: 10,
    flexDirection: 'row',
    marginBottom: gap,
    padding: gap
  },
  header: {
    fontWeight: 'bold'
  },
  text: {}
});
