import { combineReducers } from 'redux';
import contactsList from '../pages/ContactsList/reducer';
import contactPage from '../pages/ContactPage/reducer';
import auth from '../components/LoginForm/reducer';

const rootReducer = combineReducers({
  contactsList,
  contactPage,
  auth
});

export default rootReducer;
